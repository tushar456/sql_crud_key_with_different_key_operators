<?php

/*
************************************ MY SQL CODING PROJECT WITH DETAILS***********************************

SQL CRUD:-

---CREATE
---READ
---UPDATE   ---- UPDATE AND DELETE PARTE SQL ER DIFFERENT OPERATOR USE KRTE HOY. TA EKHN AMRA JANBO.
---DELETE   ----

----------------------------------------------------------------------------------------------------------


1-> CREATE SQL CODE:-   

1. create database - (key)   {create database Student}


2. set database CHARACTER - (key)  {CHARACTER SET utf8 COLLATE utf8_unicode_ci}


3.  create table - (key)  {create table user_table()}


4. ste colum for table - (key) {[this one always need to be set] - id int (13 {char limit} primary key AUTO_INCREMENT - [this one always need to be set]),}

name varchar [data_types] (60),

There are so many data_types. We can use all of those for any database.it's depend on what type of database you wanna create. 

[This is the main concept for making table and colum with different types of data_type, char limit]

----------------------------------------------------------------------------------------------------------

2-> INSERT SQL CODE:-


1. INSERT INTO `table_name` (`id`, `name`, `email` [colum name]) VALUES (give you all colum value)

REAL EXAMPLE 
{
INSERT INTO `user_table` (`id`, `name`, `email`) VALUES
(1, 'Rahim', 'rahim@gmail.com'),
(2, 'Karim', 'karim@gmail.com');

}

----------------------------------------------------------------------------------------------------------

3-> ALTER SQL CODE:-

1. in alter you cann any colum or you can mopdify anything. For now I just add a new colum 


REAL EXAMPLE:- 

ALTER TABLE user_profile add COLUMN user_id int (13) UNIQUE key after id

DETAILS:-

{ALTER TABLE [table name]->user_profile add COLUMN [colum name]->user_id int (13) [set key for your new colum]-> UNIQUE key [set placement {you can use after or first key just type key and declar colum name}] -> after id}

----------------------------------------------------------------------------------------------------------

4->  RELATIONSHIP SQL KEY:- 

ALTER table user_profile add FOREIGN KEY (user_id) REFERENCES user_table(id)

This is the basic relationship sql key code.


DETAILS:- ALTER table [table name]-> user_profile [add_key]-> add FOREIGN KEY  [FOREIGN key colum name]->(user_id) REFERENCES [REFERENCES table name and colum name]user_table(id)

----------------------------------------------------------------------------------------------------------


There are 3 types of relationship in sql.

1.one to one
2.one to many
3.many to many

Now i will show all of those relationship code step by step wih real example and  details:-

ONE TO ONE RELATIONSHIP:-

{use banglish for me}

one to one relationship add krte hle first sql code use krte hbe.

like this {ALTER table user_profile add FOREIGN KEY (user_id) REFERENCES user_table(id)}

one to one relationship e foreign key always uniqe key heti hbe.

EXAMPLE:-

user_table       [Relationship]        user_profile
    <>---------------------------------<>
    id                               user_id
     <REFERENCE KEY>                    <FOREIGN KEY> [always unique]


              --This is the basic stracture of one to one relationship--


{
project e relationship clear korar korar jonno databse ei relation kore form value  er concept korlam
id er sathe user id relationship kore user_id te id er value set kore. tar jonno modify key use korlam 

like this [UPDATE user_profile SET user_id = 1 where id = 2]

Details:- {UPDATE-> [key] [table_name]-> user_profile SET -> [key] [colum name] ->user_id = [value]-> 1 where id = 2 [id number]}
}




----------------------------------------------------------------------------------------------------------




ONE TO MANY RELATIONSHIP:-

one to many relationship add krte hle first sql code use krte hbe.

like this {ALTER table user_profile add FOREIGN KEY (user_id) REFERENCES user_table(id)}

one to many relationship e foreign key always index key heti hbe.tar jonno key add krte hbe sql code use kore

like this:-  CREATE INDEX batch_id
ON students (batch_id);



EXAMPLE:-

batch        [Relationship]       students
    <>---------------------------------<>
    id                               batch_id
     <REFERENCE KEY>                    <FOREIGN KEY> [always index]


              --This is the basic stracture of one to manyrelationship--


{ This part  is same as one to one }

----------------------------------------------------------------------------------------------------------




MANY TO MANY RELATIONSHIP:-


many to many relationship add krte hle first sql code use krte hbe.

like this {ALTER table user_profile add FOREIGN KEY (user_id) REFERENCES user_table(id)}

many to many relationship e foreign key always index key heti hbe.tar jonno key add krte hbe sql code use kore

like this:-  CREATE INDEX batch_id
ON students (batch_id);

many to many relationship krte hle alada table korte hbe tar majhe dui ta colum korte hbe.oi dui colum ke
FOREIGN KEY hishabe use krte hbe r onno 2 table er specefic id colum k REFERENCES hishabe use krte hbe.





EXAMPLE:-

course                              students
    <>                               <>
    id                               id
     <REFERENCE KEY>                    <REFERENCE KEY>


**********************************************************************************************************

                             Link_Table
        
                             CREATE RELATIONSHIP

                          
               COURSE er id->COURSE_ID      STUDENTS er id->STUDENTS_ID   
               
**********************************************************************************************************
/* LINK TABLE E XTRRA COLUM THAKLE TAKE pivot TABLE BA COLUM BOLE.


<>                                                   <>
course_id                                            students_id   
<FOREIGN KEY> [always index]                <FOREIGN KEY> [always index]


              --This is the basic stracture of many  to many relationship--


{
This time it's defferent. many to many te link table use krte hy tai id gulor value pete hole
insert use kre tader value ta dite hbe tahole value gulo link hoye jabe.


like this:-   


[INSERT INTO `admission_list` (`students_id`, `course_id`) VALUES
(2, 2),
(3, 1),
(1, 2),
(4, 1),
(2, 2),
(3, 1),
(1, 2),
(4, 1);]

}

----------------------------------------------------------------------------------------------------------
ETO TUKU OPDI ---CREATE ---READ PART USE HOISE. ERPOR SHB UPDATE R DELETE ER PART WITH OPERATOR. UPDATE R DELETE PART E ALWAYS SQL OPERATOR USE HOY.

----------------------------------------------------------------------------------------------------------

**************************************SQL DIFFERENT key OPERATOR******************************************

-WHERE
-* FROM
-SELECT 
-ORDER BY
-LIMIT
-LIKE
-NOT LIKE
-BETWEEN
-NOT BETWEEN
-AVG
-WHERE IN
-MIN
-MAX
-COUNT
-SUM
-UPPER
-LOWER
-AND
-OR
-NOT
-AS
----------------------------------------------------------------------------------------------------------

WHERE:-

where holo sql key operator. condition use krte er use drkr. sql e update or delete er time where condition use krte hoy.

DEMO CODE:-

UPDATE user_profile SET address = 'comilla' WHERE id = 1
DELETE FROM user_profile where address = 'comilla' or where id = 2


CODE DETAILS:- 

UPDATE ->(key) user_profile ->(table name) SET ->(key) address ->(colum name) = 'comilla' -> (updated value) WHERE (condition operator) id = 1. 

DELETE ->(key) FROM ->(key)  user_profile ->(table name) where (condition operator) address ->(colum name) = 'comilla' or where id = 2

where condition means kon id update ba delete korbo ta khuje bole deya sql k.


----------------------------------------------------------------------------------------------------------

SELECT and * FROM :-

suppose amar database onk list ase ekhn ami chacchi specefic kono ekta colum dekhte thik sei khetre {select from} use krte hobe. select key use kore table select kora hoy. ekhn ami jodi chai oi table er shob information dekhte tahole just {* from} key use korbo. but jodi chacchi oi table specefic colum er information dekhte tahole {colum name from}

DEMO CODE:-

SELECT * FROM user_table

SELECT name FROM user_table


CODE DETAILS:- 

SELECT ->(key) * ->(jodi table er full information chai tkhn eta use korbo) FROM ->(key) user_table ->(table name)

SELECT ->(key) name ->(jodi kono table er specefic colum er information dekhte chai tkhn just oi colum er nam dite hobe) FROM ->(key) user_table ->(table name)


----------------------------------------------------------------------------------------------------------



ORDER BY:-

jodi kono table er kono colum k nijer shubidhar jonno (A-Z) or (Z-A) ba number k (1-10) or (10-1) kore  list ta shajate chai tkhn (order by) key ta use krte hbe.


DEMO CODE:-

SELECT * FROM user_profile ORDER by address DESC -   descending (Z-A)
SELECT * FROM user_profile ORDER by address ASC - ascending  (A-Z)



CODE DETAILS:- 


SELECT ->(key) * ->(full information select key) FROM ->(key) user_profile ->(table name) ORDER by ->(key) address ->(colum name) DESC ->( key descending) ->{Z-A})


SELECT ->(key) * ->(full information select key) FROM ->(key) user_profile ->(table name) ORDER by ->(key) address ->(colum name) ASC ->( key ascending) ->{A-Z})



----------------------------------------------------------------------------------------------------------


LIMIT:-

table er colum list k limit krte hle tkhn (limit) key use krte hoy. suppose 100 ta list er majhe ami top 10 list dekhte chacchi tkhn limit key use korte hbe chaile order by condition ow use kre specefic colum er limit korte pari. 


DEMO CODE:-
SELECT * FROM students LIMIT 3
SELECT * FROM students ORDER by name ASC LIMIT 3


CODE DETAILS:- 

SELECT ->(key) * ->(full information select key) FROM ->(key) students ->(table name) LIMIT->(key) 3 ->(limitation value)

SELECT ->(key) *  ->(full information select key) FROM ->(key) students ->(table name) ORDER by (condition) name (colum name) ASC ->( key ascending) LIMIT->(key) 3 ->(limitation value)


----------------------------------------------------------------------------------------------------------


LIKE and NOT LIKE :-

databse er table e specefic word g=related gulor list dekhte chaile tkhn like key use korte hoy.


Some rules of like key:-

-WHERE CustomerName LIKE 'a%'	Finds any values that start with "a"
-WHERE CustomerName LIKE '%a'	Finds any values that end with "a"
-WHERE CustomerName LIKE '%or%'	Finds any values that have "or" in any position
-WHERE CustomerName LIKE '_r%'	Finds any values that have "r" in the second position
-WHERE CustomerName LIKE 'a__%'	Finds any values that start with "a" and are at least 3 characters in length
-WHERE ContactName LIKE 'a%o'	Finds any values that start with "a" and ends with "o"


NOT LIKE :- 

The following SQL statement selects all customers with a CustomerName that does NOT start with "a":


SELECT * FROM Customers
WHERE CustomerName NOT LIKE 'a%';


DEMO CODE:-

SELECT * FROM Customers
WHERE CustomerName LIKE 'a%';


SELECT * FROM Customers
WHERE CustomerName NOT LIKE 'a%';

----------------------------------------------------------------------------------------------------------

AND - OR - NOT :-

SELECT * FROM students WHERE name = 'tushar' or name = 'karim'

-- or operator use kore amra tushar ba karim nam khuje pabo

SELECT * FROM students WHERE name = 'tushar' and email = 'tushar@gmail.com'

-- and operator use kore tushar nam er sathe jodi  'tushar@gmail.com' match hoy tahole oitai show korbe otherwise korbe na. like true false fact.

SELECT * FROM students WHERE NOT name = 'tushar' AND NOT name = 'rahim';

-- amra jodi chai tushar r karim chara baki student der list dekhate hbe thik tokhn not operator use korte hobe

----------------------------------------------------------------------------------------------------------



BETWEEN - NOT BETWEEN :-

SELECT * FROM students WHERE id BETWEEN 4 AND 6 ORDER BY id

-- suppose kono table e 10 ta student er id ache, ekhn ami chacchi 4 to 6 opdi id gula dekhte sei khetre between use korbo.


SELECT * FROM students WHERE id NOT BETWEEN 4 AND 6 ORDER BY id

-- suppose kono table e 10 ta student er id ache, ekhn ami chacchi 4 to 6 opdi id gula bad diye baki id gula dekhte sei khetre not between use korbo.

(between and not between id, name mani je kono colum er jonno hte pare, eta depend kore nijer project er upor.)

----------------------------------------------------------------------------------------------------------

UPPER and LOWER :-


SELECT UPPER(name) 
FROM students;

-- kono table er colum er vitor je information ache ta jodi shb capital letter hishabe dekhte chai sei khetre eta use korbo.


SELECT lower(name) 
FROM students;

-- kono table er colum er vitor je information ache ta jodi shb small letter hishabe dekhte chai sei khetre eta use korbo.

----------------------------------------------------------------------------------------------------------


AVG - COUNT - SUM :-



SELECT SUM(id)
FROM students;

-- The SUM() function returns the total sum of a numeric column.

SELECT AVG(id)
FROM students;

-- The AVG() function returns the average value of a numeric column.

SELECT count(id)
FROM students;

-- The COUNT() function returns the number of rows that matches a specified criteria.

(avg and sum key always work for numeric value or colum)

----------------------------------------------------------------------------------------------------------

MIN - MAX :-

SELECT MIN(id) FROM students

-- The MIN() function returns the smallest value of the selected column.

SELECT MAX(id) FROM students

-- The MAX() function returns the largest value of the selected column.


----------------------------------------------------------------------------------------------------------

WHERE IN:-

jodi ami kono table er kisu specefic colum er value dekhte chai tahole where in key use korbo.

DEMO CODE:-

SELECT *
  FROM students
 WHERE name IN ('tushar', 'karim', 'abul')

CODE DETAILS:-

SELECT ->(key) * ->(full information select key) FROM ->(key) students ->(table name) WHERE ->(key) name ->(colum name) IN ->(key) ('tushar', 'karim', 'abul') -> (jeshob value r tader information show korbe)

----------------------------------------------------------------------------------------------------------

AS:-

suppose ami kono colum jar nam studentName. ekhn ami dekhte chacchi student der colum taw all capital letter and tar jonno amake condition apply korte hobe r sathe ami etaow chacchi oi colum er nam ta jno Students_name evabe show hoy, emn condition er jonno as key use korte hbe. eta je kono sql er kaj e use kora jay.


DEMO CODE:-

SELECT upper(name) AS uppercasename FROM students


CODE DETAILS:-

SELECT ->(key) upper(name) ->(condition with colum name) AS ->(key) uppercasename (new colum name) FROM ->(key) students ->(table name)

********************************************END***********************************************************

















*/